title: Subplot
class: animation-fade
layout: true

.bottom-bar[
{{title}}
]

---

count: false

# Leader slide

- Press 'b' to toggle 'blackout mode'
- Press 'p' to toggle 'presenter mode'
- Press 'c' to create a clone of this window which will keep up
- Press left/right to move around slides
- The next slide is where the presentation begins

???

This is where presenter notes appear

---

count: false
class: impact

# {{title}}

## Expressing argumentation in a validatable way

## Daniel Silverstone <br /><tt>&lt;dsilvers@digital-scurf.org&gt;</tt>

???

- Hello
- My name is Daniel Silverstone, and I've been working in and around open
  source software for over 25 years.
- In this talk, I will aim to cover:
  - A brief introduction to Safety argumentation documentation
  - We will define what Requirements, acceptance, and verification criteria are
  - We'll talk about rendering verification criteria as scenarios
  - We'll have a go at turning some argumentation into scenarios
  - And finally we'll cover what subplot is, and thus how it might help with all this

---

class: impact2
count: false

## Disclaimers and thanks

???

- 'Expert' bandied around a little too freely in the world these days
- While I know some of the safety communities lingo etc. not an expert
- I am employed by Codethink, and will use an example of theirs in the talk
- But I am not here for Codethink, and this talk is not sponsored by Codethink
- Thanks are due to a few of my colleagues, Paul A. Shaun M. for helping with safety content
- Thanks also due to my FOSS partner in crime, Lars Wirzenius who is the other half of the Subplot project
  currently.
- _Take a breath_
- So I see a problem in software safety - namely that we're not seeing automated
  validation of assertions in safety argumentation, and I'd like to talk to you about
  one way I see of improving the situation

---

layout: true
title: Safety argumentation documentation

.bottom-bar[
{{title}}
]

---

# What is a safety case

> The purpose of a safety case is to provide a clear, comprehensive and defensible
> argument, supported by evidence, that an item is free from unreasonable
> risk when operated in an intended context.

???

- ISO26262 definition of a safety case
- _Read the safety case_
- Three principle parts
  - the safety goals and related safety requirements
  - the safety argument
  - the work products (evidence)

---

class: impact2

.rborder[
.iso-26262-software-level-overview[
]
]

???

- Since ISO26262 says 'work product equals evidence' it can become bewildering
  to navigate.
- Evidence could be anything from test suite results to an indication of
  a document review etc.
- Particularly bewildering if your process doesn't exactly match the template
  for ISO26262 which relies on a very waterfall, or V, model of development.
- Rather, more realistically ISO26262 offers up a number of classes of evidence
  that you might need in order to build an effective argument,
  rather than an exhaustive list of what evidence you must have.

---

count: false

# Can we boil that down a little?

- Analysis and Description
- Assertion
- Evidence

???

- So let's try and boil down the essence of this approach a little
  to see what we get.

---

# Can we boil that down a little?

- **Analysis and Description**
- Assertion
- Evidence

???

- Starting from an analysis of the system, and your high level safety objectives
- You can construct descriptions of each component and the safety cases around
  those components.

---

count: false

# Can we boil that down a little?

- Analysis and Description
- **Assertion**
- Evidence

???

- We can get from those descriptions to some assertions which if met would indicate that the system meets
  the safety case

---

count: false

# Can we boil that down a little?

- Analysis and Description
- Assertion
- **Evidence**

???

- To convince others that the assertions are met, we collate a set of Evidence

---

# Doing this in practice

- **Analysis**
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST

???

- Various possible ways to analyse a system from the point of view of trying to understand its safety properties

---

count: false

# Doing this in practice

- Analysis
  - **FMEA / FMECA**
  - Fault Tree
  - STAMP/STPA/CAST
- Description
- Assertions
- Evidence

???

- Failure Mode and Effects Analysis
- Failure Mode, Effects, and Criticality Analysis
- Commonly used approach which uses inductive reasoning to synthesise a full system view from the bottom up
- Originated in the US military in the 40s, it's a solid and well understood approach to understanding the risks in a system
- Good for exhaustively cataloging faults and local effects

---

count: false

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - **Fault Tree**
  - STAMP/STPA/CAST
- Description
- Assertions
- Evidence

???

- Fault tree analysis
- Developed in Bell Labs in the 60s _for_ the US military
- Top-down analytical approach, uses common boolean logic combinators to express the analysis
- Good for understanding how a system responds to multiple simultaneous faults but not at finding all possible faults
- Commonly used together with FMEA/FMECA

---

count: false

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - **STAMP/STPA/CAST**
- Description
- Assertions
- Evidence

???

- Systems-Theoretic Accident Model and Processes
- Systems Theoretic Process Analysis
- Causal Analysis based on STAMP
- Much newer approaches first described in Nancy Leveson's 2011 book "Engineering a Safer World"
- Models systems as components/processes, control actions, and feedback pathways
- Can only identify hazards at the level of the analysis
- But can drill into system components to whatever depth is necessary to identify all hazards
- Requires careful attention to ensure both sufficient analysis is done, and that the analysis
  does not go so deep as to become impractical.

---

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST
- **Description**
- Assertions
- Evidence

???

- Having run the various analyses we've just described, you are left with a number of documents which
  explain how a system might fail, and to a greater or lesser extent, how that might affect the system.
- The exact format for writing up these hazard conditions and effects will vary from approach to
  approach, as well as from company to company; however no matter what, the description will cover
  the hazard conditions, the likelihoods, the criticality of the failure, and so on.
- Commonly the description must be comprehensible to the people ultimately responsible for confirming
  the safety properties of the system. This might be for certification, Quality Assurance, or any
  other reason.
- Also the description has to be comprehensible to the people responsible for formulating the assertions
  for the system.

---

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST
- Description
- **Assertions**
- Evidence

???

- The assertions for a safety argument are the statements which, if met, the author of the assertions
  believe will evidence the safety properties of the system.
- Not all assertions will be directly related to things traditionally thought of as safety though.
- For example, you might have determined in your analysis that a hazard could be that the compiler
  sometimes, unpredictably, produces bad machine code for a given good source code input.
- You might make an assertion that for any given code input the compiler will _always_ produce the
  same machine code output, permitting a later assertion that the machine code does the right thing
  to rely on the fact that it will never cease doing the right thing just because of the compiler.

---

# Doing this in practice

- Analysis
  - FMEA / FMECA
  - Fault Tree
  - STAMP/STPA/CAST
- Description
- Assertions
- **Evidence**

???

- If everyone reading the analysis, description, and assertions, agrees that with the assertions being
  met the system safety properties are sufficient, then the last step in the argumentation is to gather
  and provide Evidence
- Evidence is typically gathered together by a person, to the extent necessary to demonstrate that the
  assertions are met. Then by simple inspection of the collated documentation, the properties of the
  system can be confirmed.
- Sadly this does not (a) scale to large systems without having large numbers of people working on this,
  or (b) scale to a continuously updating system without having to continuously re-do this work.
- Modern systems which have to be safe are also more and more likely to contain significant quantities of
  computer software which in an always-on always-connected world implies semi-continuous updates etc.

---

class: impact

## Deterministic Construction Service

???

- Codethink spoke toward the end of last year, at the ELISA workshop, about their
  Deterministic Construction Service service and how it achieved ISO26262 certification.
- We'll use this as an example of how we can start to bridge the gap between traditional
  safety argumentation analysis and evidencing, and the well trodden paths of good
  software engineering practice

---

count: false

# Deterministic Construction Service

- **A design pattern for reproducible software construction**
- A way to verify this reproducibility for a given set of inputs in a given DCS instantiation
- Using these properties to inform verification and impact analysis
- Automating all of this into a continuous integration workflow

???

- I mentioned earlier the idea that one part of determining that software is safe could be asserting
  that for the same inputs you always get the same outputs.
- This idea is well known in the open source software world, and is referred to as reproducible
  builds, or bit-for-bit reproducibility

---

count: false

# Deterministic Construction Service

- A design pattern for reproducible software construction
- **A way to verify this reproducibility for a given set of inputs in a given DCS instantiation**
- Using these properties to inform verification and impact analysis
- Automating all of this into a continuous integration workflow

???

- Naturally when we think of reproducibility in software we have to take into account not
  only the traditionally thought of inputs (source code, libraries, etc) but also the
  wider context in which software systems are constructed.
- While it sounds quite weasel-wordy, DCS demystifies this by simply treating the DCS instance
  as part of the inputs, covering not only the environment for the construction, but also
  the rules and processes governing it.

---

count: false

# Deterministic Construction Service

- A design pattern for reproducible software construction
- A way to verify this reproducibility for a given set of inputs in a given DCS instantiation
- **Using these properties to inform verification and impact analysis**
- Automating all of this into a continuous integration workflow

???

- In a trivial sense, if a change in inputs has no effect on the constructed software
  artifacts, then it has no impact on the behaviour or other properties of the system.
- In this way, a basic check for "did this bug fix actually affect the output" can help
  to evidence if work was of value.
- Ditto, if a change in one component does not affect any other component, then there can
  be a reasonable reduction in total system testing required as a result.

---

count: false

# Deterministic Construction Service

- A design pattern for reproducible software construction
- A way to verify this reproducibility for a given set of inputs in a given DCS instantiation
- Using these properties to inform verification and impact analysis
- **Automating all of this into a continuous integration workflow**

???

- Again, as I mentioned earlier, modern software systems change a lot, perhaps even all the
  time.
- By integrating all the validation and impact analysis steps into a continuous workflow
  you can be more confident that changes your developers are making do not adversely affect
  safety properties of the system.
- You can be more reponsive to the changing needs of the users of your system
  or conditions in which your system is existing.
  For example, security updates to internet connected cars.
- You need that software update to go out same day or next day

---

class: impact
count: false

### Deterministic Construction Service

# Necessary, but not sufficient.

???

- All of the above is a necessary part of getting toward making automated safety assertions
  about software systems
- However at the moment, DCS cannot make any assertions about the safety properties of the
  software built using it
- Instead it provides the first step - confidence that if we do analyse the software systems
  and make assertions, that nothing about the processes of converting that to production
  software could invalidate those assertions.

---

# Automatic evidencing of argumentation

## Deriving trust

- **What can DCS do now?**
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidence

???

- So DCS can provide us with a process of generating automatic Evidence which go alongside
  the argumentation DCS makes about the process of software build and integration.
- The Evidence can be automatically constructed and thus the human portion of aggregating
  Evidence and collating the argumentation can be eliminated, saving time, effort, and
  reducing risk.

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- **How does that play into validation?**
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidence

???

- In the safety world, we need to establish a sequence of trust from established norms
  through to the system in production
- This chain involves certifications, qualified people, systems, and processes,
  argumentation, and the Evidence supporting that argumentation

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - **Standards and certification**
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidence

???

- Ultimately, trust starts in consensus around standards such as ISO26262

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - **Qualified people, systems, and processes.**
  - Argumentation
  - Evidence

???

- From there, we look to certify a combination of people and processes
  such that we can draw a line of trust from the standard through those people and processes

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - **Argumentation**
  - Evidence

???

- Those people and processes are then entrusted to produce analyses
- From there, they can describe the safety properties of systems, identify hazard conditions etc.
- They (both people and processes) then produce/provide argumentation around those properties and conditions
- The assertions that are contained in the argumentation have trust derived from the creators of them.

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - **Evidence**

???

- Finally, either by automation from the processes, or by dint of work done by qualified people
  we can construct Evidence to support the argumentation
- As someone outside of the process, we can derive a level of trust that the Evidence shows
  the safety properties of the system by following the trust chain.
- We trust that the safety standard is good and if followed will result in a safe system
- We trust that the qualified people, systems, and processes meet that standard and that they
  will do the "right thing" whatever that may need to be
- We trust that the constructed argumentation was done in conjunction with those trusted people,
  systems, and processes
- And we trust that the Evidence generated supports the argumentation by dint of trusting
  the whole chain to tell us if it doesn't.

---

count: false

# Automatic evidencing of argumentation

## Deriving trust

- What can DCS do now?
- How does that play into validation?
  - Standards and certification
  - Qualified people, systems, and processes.
  - Argumentation
  - Evidence

## Turtles all the way down

???

- If your high level documents (standards) tell you how to know if the next level of documents
  are good, and those documents tell you how to know if the next level is good etc.
- Then you can chase this trust all the way down from the standards to the individual
  components (software or otherwise) in a system
- And then you can chase the Evidence back up the trust chain, verifying at each level, until
  eventually you can determine your trust in the system in question

---

layout: true
title: Requirements and acceptance criteria

.bottom-bar[
{{title}}
]

---

# What are requirements?

- High level vs. low level
- Functional vs. non-functional

???

- So let's talk about requirements
- _Pause for a moment_
- Requirements are statements about how a system must or must not behave
- At a high level these might be business need statements, and at a low level
  they may state exactly the angle of a screw thread, or a singular behaviour
  or property of a software component.
- While the term applies across the full scale, typically requirements are
  used in a formal sense only in engineering design, and we use different words
  for the higher levels of system specifications, such as 'business needs'.
- Functional requirements specify capabilities, behaviours, and information that
  any solution must have or exhibit in order to to be effective.
- Non-functional requirements specify properties, constraints, or characteristics
  of solutions. For example, reliability, availability, and so on.

---

# Validating requirements

- **Analysis**
- Inspection
- Demonstration
- Automated criterion

???

- The act of determining the requirements of a system is often called requirements
  analysis. This starts from high level requirements and then breaks those down into
  further requirements.
- The high level requirement is then said to be met if all the broken down requirements
  are met by the system
- We call this 'Validation by analysis' and this forms the backbone of a lot of requirements
  validation in use today.

---

count: false

# Validating requirements

- Analysis
- **Inspection**
- Demonstration
- Automated criterion

???

- Sometimes a requirement can only be verified to be met by means of inspecting something
- For example, a requirement that an interface be aesthetically pleasing could perhaps
  be verified by means of some trusted party looking at the interface and making a statement
  about its aesthetics.
- This doesn't _have_ to involve exercising the system under test if the inspection is of
  some other part of the system or processes.

---

count: false

# Validating requirements

- Analysis
- Inspection
- **Demonstration**
- Automated criterion

???

- Sometimes called 'human testing'
- Verification by demonstration involves using the system as intended and showing that
  the properties of the running system match the requirements set out.
- An example here might be showing that operating an electric window switch in certain
  conditions (e.g. the ignition switch being on) results in the window moving as expected.
- Sometimes this can be quite detailed, involving controlled conditions etc. at which
  point this may be referred to as 'Test' rather than demonstration

---

count: false

# Validating requirements

- Analysis
- Inspection
- Demonstration
- **Automated criterion**

???

- Automated criteria for validating requirements are often referred to as 'test cases'
- These can be executed automatically by some kind of testing system or harness
- Very common in software systems, commonly simply called 'tests' in that situation.
- Multitude of kinds of tests for software systems, including but not limited to:
  - unit tests
  - integration tests (various levels, but open box)
  - system tests (kind of integration, but treated as a closed box)

---

class: impact

# Stakeholders

## Who cares about validation?

???

- It's reasonable, when considering requirements and validation, to think about who
  in particular cares about each requirement.
- The particular stakeholders who can be said to have an interest in any given requirement
  may dictate the manner in which that requirement is validated.
- Let's take a walk through the stakeholders of a software product
- We are deliberately excluding any stakeholders from outside of the company we present
- For example we will not consider the customers of the company, or the users of the product.
- In our example we're going to exaggerate the distance between the understanding
  of someone on the board of a company, and the individual programmer writing code.
- The actors in this walkthrough are very self-absorbed and find it hard to look
  outside of their particular bubble. This is not completely realistic but let's
  us consider different points of view.

---

# Stakeholders in validation

- **Company board, CEO**
- Product owner
- System architect
- Development teams
- Individual engineers

???

- While it may sound odd, just like trust chains start at the very top, so does
  the care about requirements.
- The board of a company, the CEO/CTO/etc. must have some level of stake in the
  requirements of a product their company is producing. Their stake is likely
  at the level of business needs, but they will need to know that the needs
  will be met by the product.

---

count: false

# Stakeholders in validation

- Company board, CEO
- **Product owner**
- System architect
- Development teams
- Individual engineers

???

- The owner of a product is responsible for its definition and success. They
  are responsible for ensuring that the product design etc. meets the business
  needs and that the mechanisms of validation for that are understood by the board.
- The product owner is also responsible for ensuring that the system architects
  understand the requirements well enough to analyse the system and define
  further requirements and system design.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- **System architect**
- Development teams
- Individual engineers

???

- System architects are often the first point at which non-functional requirements
  and very high level functional requirements begin to be broken down into further
  requirements, which stand a chance of automated validation,
- Although, any requirement at any level _could_ be automatically validated in some fashion.
- A system architect may not need to understand how the product might meet the business
  needs of the company, but they will have had those effectively broken down and
  communicated as product requirements by the product owner.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- System architect
- **Development teams**
- Individual engineers

???

- Development teams receive requirements defined by the system architect and through
  analysis and design, determine how to build software which meets those requirements.
- They likely understand the full product at some level, but may only be responsible
  for building some sub-part of it.
- This is likely the point at which requirements begin to take on quite technical
  forms, tightly defining individual component behaviours.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- System architect
- Development teams
- **Individual engineers**

???

- The individual engineer is likely working from well defined low level requirements
- They may have to implement particular software components to specific standards
  protocols etc.
- In an extreme sense, they may have no concept of how that component fits into the
  product as a whole. This is quite common when software component development is
  outsourced.

---

count: false

# Stakeholders in validation

- Company board, CEO
- Product owner
- System architect
- Development teams
- Individual engineers

## Comprehension goes one up/one down

???

- As a result, when we look at this heirarchy of responsibility and understanding
  we see that comprehension of requirements can go one level up or down only.
- i.e. the product owner needs to comprehend the board's needs in order to present
  requirements to the system architect
- but they will also need to comprehend the system architect's Evidence in order to
  be satisfied that the requirements they passed down are met, so that they can then
  generate Evidence for the board above them.
- All these levels could seem very onerous, and in some cases they really can be,
  they can also be much less well defined.
- In reality, while the difference between the levels may be less pronounced than in
  our exaggerated example world, the heirarchy will not usually be so clear-cut and linear,
  especially when you get other departments involved in system design, for example
  a cybersecurity department may place requirements on a system with no regard to
  the product's functionality per-se, but in order to meet some business needs around
  not producing insecure software.

---

class: impact

## Acceptance criteria

## and

## Verification criteria

???

- Another term you'll hear bandied around is 'acceptance criteria'
- In some sense, particularly for our purposes, this is just another word for requirement
- Typically high level, often from the perspective of a user of the system
- Usually defined by very high level stakeholders, or at least approved by them
- Can still be automatically validated, at least sometimes.
- Usually coupled with 'verification criteria' which is a description of how the
  acceptance criterion is verified to have been met. Effectively a 'test case'.

---

layout: true
title: Verification criteria as scenarios

.bottom-bar[
{{title}}
]

---

class: impact

## Verification criteria

???

- Let's take a moment to focus on verification criteria
- I will focus pretty much only on software, but in theory
  all of this can apply in some sense to hardware systems
  or processes etc.

---

# Kinds of testing

- Unit testing
- Integration testing (open-box)
- System/closed-box testing
- Inspection

???

- Unit testing treats individual isolated components of a system as a unit of functionality
  and verify their behaviour. In the extreme this may involve calling individual software
  functions to verify they behave as intended.
- When the interaction between units needs to be verified, we move to integration testing.
  We refer to this as open-box because typically the testing harness will have some privileged
  access to internal state of software components in order to do this verification.
- System, or closed-box testing, is a higher level variant on integration testing, which verify
  the behaviour of an integrated aggregate of components without privileged access to their
  internal states. Verification is done by observing (in an automated fashion) the behaviour
  of the system as a whole
- Verification by inspection, or user testing, is still necessary in some cases, though it is
  much harder to automate.

---

class: impact

## The rise of agile methodologies

### User stories

???

- While what I've spoken about so far is quite traditional, more and more software teams are
  starting to use a variety of agile methodologies including capital-A Agile, Lean, etc.
- The big thing which came out of agile which we are interested in today are user stories.
- Often presented in the form Persona - need - purpose
- e.g. "As a visitor, I want to buy a ticket, so that I can watch a film"

---

class: impact2

## Cool as a cucumber

# Given, When, Then

???

- Codifying the user-story approach to expressing requirements led to the creation of
  the Gherkin language
- This is a standard for expressing verification criteria in a scenario form, enabling
  the reader of the criterion to understand the setup, action, and checks which comprise
  the validation of the requirement.

---

## As a visitor, I want to buy a ticket, so that I can watch a film.

```
given a cinema booking system
and an empty shopping cart
when the user chooses a film
then the shopping cart contains a ticket for the film
when the user selects checkout
then the ticket is sold
```

???

- Scenarios describe the setup of the situation (given)
- Actions which are performed (when)
- and verifications of the results (then)
- Each step in the scenario can be customised to the system being verified
- Typically steps are designed to be generic enough that more criteria can be
  written using a combination of existing steps, rather than defining unique
  steps each time

---

## As a stakeholder, when a scenario exists, then I can understand it

- **Looking "upward"**
- Looking "down"
- Test code is the best code

???

- When a test owner writes a scenario for meeting a requirement, it should be
  clear to the author of the requirement what the scenario needs/assumes, what
  it will do, and how it will check the result of that.
- This clarity ensures that stakeholders further "up" the chain can comprehend
  tests and accept that they will validate their requirements, without actually
  needing to understand how they will do that in detail

---

count: false

## As a stakeholder, when a scenario exists, then I can understand it

- Looking "upward"
- **Looking "down"**
- Test code is the best code

???

- The test owner write scenarios using steps which are defined in conjunction with
  the people responsible for actually implementing the test.
- This means that the test owner can trust that the programmer will understand
  the purpose of the steps, and they can both agree that the code to implement
  the step matches the intent of the scenario
- Nominally it also means that the programmer need not understand the wider product
  codebase fully, nor the requirements set, to be effective at their job.

---

count: false

## As a stakeholder, when a scenario exists, then I can understand it

- Looking "upward"
- Looking "down"
- **Test code is the best code**

???

- Some years ago, I wrote a set of "trusims" of software.
- Two of them were: "If you don't know why you're doing it, you shouldn't be doing it."
  and "If it's not tested, it doesn't work"
- The former is a restatement of the idea that software products should only contain
  code needed to meet requirements of the system. If there's no requirement for code
  to exist, it shouldn't.
- The latter then covers the idea that if you wrote the code, you'd best be certain that
  it works, and the best way to do that is to have a test for that feature.
- Sadly, test code is often only verified to work by dint of running it against a system
  and then making a decision on test failure as to whether the test is bad, or the system
  under test is at fault.
- Given that, your test code needs to be patently clear and correct such that it can be
  validated by inspection.
- The scenario approach to testing permits the higher level test to be validated independently
  of the code implementing the test, meaning that more people can be confident of the correctness.

---

layout: true
title: Turning argumentation into scenarios

.bottom-bar[
{{title}}
]

---

count: false
class: impact

# Argumentation as scenarios

???

- Let's take a look at the benefits of rendering argumentation as scenarios,
- how we might do that
- and what different perspectives might see as the value it brings

---

# Rendering argumentation as scenarios

- **Automatic validation**
- Continuous compliance checking
- Reduction in validation by analysis/inspection

???

- The biggest and most obvious value, from my perspective at least, is that the validation
  can be automated.
- Critically this is not suggesting that scenarios result in automatic construction of argumentation
  but rather that creating and matching Evidence to assertions could be automatic
- Anything which is automated has a lower risk of human error and can be fitted into modern
  system construction pipelines.

---

count: false

# Rendering argumentation as scenarios

- Automatic validation
- **Continuous compliance checking**
- Reduction in validation by analysis/inspection

???

- By writing scenarios about ensuring that processes are followed properly, the Evidence
  produced by those scenarios inherently supporting the assertions for those processes,
  you get something truly great.
- Automatic execution of scenarios which confirm that processes, systems, and activity is
  complying with standards is the core of ensuring that any modern software project could
  be safe.

---

count: false

# Rendering argumentation as scenarios

- Automatic validation
- Continuous compliance checking
- **Reduction in validation by analysis/inspection**

???

- Again, from the perspective of "Humans are fallible" if a computer can take the carefully
  constructed scenarios and by executing them provide confidence that each level of a system's
  trust model is correctly being handled, there is a significant reduction in the human cost
  to making safe systems.
- While it doesn't mitigate all the analysis, since clearly some will have to happen in order
  to (a) create the sub-level and (b) satisfy the stakeholders that if the scenario passes then
  the assertions are met, it does reduce the complexity of the analysis.
- No longer will someone checking if a process is being followed need to inspect large amounts of
  evidence to verify that they support an assertion, instead they inspect the scenario, rely on
  the trust chains to be satisfied that the Evidence will be useful, and then take the outputs of
  the automated process.

---

class: impact
count: false

# Examples

???

Before we begin with a couple of examples of some deliberately basic arguments
along with how they might be rendered as scenarios, let me reiterate:

1. I am not a safety expert
2. These examples are really simplistic, not rigorous in any sense and are meant
   purely to illustrate an approach.
3. I invented these scenarios basically of whole cloth, with some help from my
   colleague Paul.

---

# A process example

> In order to prevent uncontrolled changes to the software of the system,
> there is in place a change management process which regulates how changes
> can be made to the software.
> <br><br>
> To this end, we assert that the revision control system in use enforces this
> process.
> <br><br>
> As evidence we present an analysis of all changes within the system, demonstrating
> that all changes meet the process definition for some given software release.

???

- Given that, let's speak a little about an example of how I imagine a process
  argument might be stated.
- _Read through this, to ensure that people who are partially sighted are able to
  at least understand_
- This idea is one which you find in multiple open source processes around securing
  software supply chains, and I believe something akin to this is present in DCS.
- The concept here is something you are all likely familiar with - that without
  change control you cannot begin to make any guarantees about the system.
- What I don't try and define here is what the process is, nor how it is regulating
  change, that's not interesting at this level.

---

# A process example as scenario

> <pre>
> given a release to verify
> and a set of repositories which comprise that release
> and a set of rules for what comprises a valid permissible change
> when all commits present in the release are examined
> then all commits obey the rules
> </pre>

???

- We only concern ourselves with the assertion here. The scenario here is replacing
  the assertion from the previous slide
- That is that we assert the revision control system is enforcing the process.
- _Read the statements_
- What I'm hoping you can all agree is that the assertions effectively say the same
  things, though we've also managed to encode some of our evidences statement
  as well.
- But also I'm hoping that you'll agree that it is just as comprehensible to a human
  as the prose was.

---

# A component example

> To ensure that the driver is able to rely on the reversing camera being available
> when it is needed, it must be displayed within 500ms of the reverse gear being selected.
> <br><br>
> To this end, we assert that whenever the gear-selection message is received by the
> center console display, should it indicate that reverse gear has been selected, then the
> time from message reception to the display of the reverse camera input to the user shall be
> be no more than 500ms.
> <br><br>
> As evidence we present system test results for the release software showing that the
> timing for the reversing camera being displayed was within the limits specified.

???

- Now let's look at an assertion which is a little closer to something we might expect
  a programmer to deal with.
- _Read through the text again_
- This idea, again, should be pretty comprehensible to anyone who has ever driven
  a car which has a reversing camera. Users of them pretty quickly grow to be at least
  mostly dependent on them, especially in low light or tight driving situations.
- As such, we want to ensure that the user doesn't have to wait for the camera to display
  when they choose reverse.
- Now, I'm not claiming this one argument alone is sufficient to assert the safety of the
  reversing camera, but it is likely that at this level any scenario set which was
  worth its salt would cover things like camera-to-display latency, fault injection
  on things like the gear selection signal, etc.

---

# A component example as scenario

> <pre>
> given a functioning test rig
> and the release software loaded into it
> when a reverse-gear selection signal is injected
> and a display snapshot is taken 500ms later
> then the reverse-camera input is visible on the display
> </pre>

???

- Setting the incompleteness problem aside, let's render this as a scenario too
- _Read through the scenario_
- Again, hopefully you can all agree that this effectively represents the assertion
  and captures the essence of the evidence statement as well.
- Also, at this level, I'm imagining those of you closer to the code-face might be
  imagining how you might implement some of this.

---

class: impact
count: false

# Mining at the code-face

???

- Let's take a very brief look at how we might go about building implementations for
  the tests we just described.
- I'm not going to take you through implementing a whole rules engine, nor something
  capable of taking screenshots of a vehicle test rig
- But let's look at one or two of the statements from before and have a go at looking
  at implementation

---

> `… a set of rules for what comprises a valid permissible change`

```python
def acquire_change_policy_ruleset():
  release = context["policy_version"]
  repos = context["repos"]
  ruleset_repo_path = repos.repo_base("policy/changes")
  ruleset_repo = GitRepo(ruleset_repo_path)
  ruleset_repo.checkout_tag(release)
  compiled_ruleset = Ruleset.compile(ruleset_repo_path)
  context["change_policy_ruleset"] = compiled_ruleset
```

???

- Using python-like pseudocode, this is _NOT_ a subplot scenario step function
- _Talk through the code_
- Here you can see the concept that there is some kind of test context
- It likely had things set up by previous steps in the scenario
- The function does one simple sequence of actions, it ensures the changes policy repository
  is on the right version, and loads it into the context.
- In reality, most step functions would be written to be more generic than this.
  For example, we might be able to capture the word "change" and then
  alter the step function to be able to load different policies based on that
  capture

---

> `… the reverse-camera input is visible on the display`

```python
def check_display(expectation, visibility):
  current_screenshot = context["screenshots"].most_recent()
  comparison_image = load_image(expectation)
  showing = current_screenshot.compare_with(comparison_image)
  if visibility == "visible" and not showing:
    return StepError(f"{expectation} was not visible")
  elif visibility != "visible" and showing:
    return StepError(f"{expectation} was visible")
```

???

- This time the step function is clearly meant to take two parameters which
  we can reasonably expect to be "reverse-camera input" and "visible"
  for this particular step
- You can see how by parameterising steps, you make them more flexible
  and able to be used in the future in more complex scenarios without
  needing extra programming effort.
- But enough pseudocode for now, let's move on

---

# Perspectives in deriving value

- **CISO/CTO/CEO**
- Low level systems engineer
- Turnabout is fair game?
- ???

???

- If we think about the process example, our original argument prose was meant to be
  consumed by someone who doesn't have to care what the process is, or how it's enforced,
  merely that it exists and that it does the right thing.
- While initially the particular shape of a Gherkin like scenario might be alien to such
  a person, they would easily be able to understand the scenario version of the assertion
- And through the chains of trust we established earlier, they can be confident that those
  who do understand the detail will have ensured that if the scenario passes, this means
  that it is doing what it appears to be claiming to do.
- Thus they can derive just as much, if not more, trust value from the scenario as from
  the prose assertion. Since they likely could not comprehend the evidences presented in
  either case, this is, I hope, sufficient to suggest that at whatever level a non-engineer
  encounters these kinds of scenarios, they can derive the value and understanding they need.
- Naturally in a full system, there'll be many many more scenarios, no one person need
  understand them all, and coupled with the trust model, we can reach a point where each person
  at this end of the chain can derive the value they need without understanding the automated
  processes to the full.

---

count: false

# Perspectives in deriving value

- CISO/CTO/CEO
- **Low level systems engineer**
- Turnabout is fair game?
- ???

???

- Most engineers are typically entirely uninterested in process where it does not
  immediately impact them, however they definitely like it when they're given clear
  requirements with obvious mechanisms to test if they've been met.
- As such, engineers derive more value from the lower level scenarios since they show
  something they likely feel they can test.
- Engineers particularly like if those verification criteria are automated and part of
  a continuous integration and test pipeline.

---

count: false

# Perspectives in deriving value

- CISO/CTO/CEO
- Low level systems engineer
- **Turnabout is fair game?**
- ???

???

- If the CEO reads the lower level scenario, then they likely can still derive
  an understanding of what it means and why it's important. Though they likely won't
  have quite the same appreciation for it as a low level engineer might.
- Ditto the low level engineer can likely read and understand the high level scenario
  though they likely won't care about how/why it exists. They'll already be used to
  the rules in question, and won't be thinking at the process level about why we need
  to know the rules are obeyed.
- So there's value at every level, though likely only scenarios at a level where the
  reader actually cares will provide the full value possible.

---

count: false

# Perspectives in deriving value

- CISO/CTO/CEO
- Low level systems engineer
- Turnabout is fair game?
- **Test engineers**

???

- Of course, there have to be engineers who are writing these step functions, and they
  likely need to appreciate the requirements of the scenario statements at whatever
  level they are working. Fortunately they should have the original scenario author
  as reference.
- Interestingly, the step function author may not need to know much more than the
  very tight specification for the step function's behaviour, though if they understand
  more then they may be able to write more flexible functions which the scenario
  document owner can then use in more scenarios later.
- More often than not, the step function author will be fairly close to the authors
  of the system which is being verified, ensuring good comprehension.
- Ideally the scenario author ought to be able to understand the step function code
  or else have an established trust chain to the step function authors.
- The value test engineers derive is typically isolated only to the information they
  need to establish the step functions, but that's OK.

---

layout: true
title: What is subplot

.bottom-bar[
{{title}}
]

---

class: impact

## Long ago, in a galaxy far far away…

???

- Approximately ten years ago, three friends, in a restaurant, were chatting over their lunch.
- They were discussing how unpleasant it was to write the tests for a tool they were working on.
- One of them had heard about Gherkin and scenarios and they liked the idea
- Unfortunately at the time, the only implementations they knew about were either in Ruby
  (which wasn't in use on the project) or else were aimed at web applications (which wasn't what
  our intrepid friends were developing)
- They set out to write and use a simple scenario testing tool which they called `yarn`
- Over the next few years, they wrote a lot of good test suites for a variety of software,
  proving out that the concept of scenario testing worked for UNIX CLI tools
- Sadly they mostly produced bad documentation along the way as well,
  as is the wont of many software engineers.

---

class: impact

## Some time ago, but this time in our galaxy…

???

- Five years ago, two of those friends sat in front of a whiteboard and discussed what they wanted
  `yarn` to be long term, thinking about clever features, and ways to do more complex testing.
- They spent the next two years, on and off, experimenting with these ideas; writing more good
  test suites, and still producing frankly awful documentation.
- Late in 2019, they performed some basic user surveying, decided that simplicity was key,
  documentation was the most valuable / primary goal, and thus they pivoted to the ideas which became Subplot.
- They set the goals of Subplot (high quality documentation generation, with automatic testing as a secondary feature)
- Six months saw them produce something usable in projects other than Subplot
- Another year got them to the first alpha version
- Now approaching the second alpha release, we learn more and more through using the tool

---

# Some features of Subplot

- **Documentation**
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Subplot's initial, and nominally primary, purpose is that of documentation
- _wait a few seconds_
- Subplot documents are exactly that, documents, likely primarily composed of
  prose, with images and scenarios as appropriate.
- _wait a few seconds_
- Subplot documents are meant to be revision controlled, for example in a
  Git repository, and so are plain text documents in terms of inputs to Subplot's
  document generation feature.

---

count: false

# Some features of Subplot

- _Documentation_
  - **Markdown text**
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Like Yarn, and countless other tools, Subplot uses Markdown as its input language.
  Nominally this could have been asciidoc or reStructured text, but the authors of
  Subplot are most comfortable with Markdown
- Markdown documents are fairly easy to read in their source form because rather than
  tags like HTML, Markdown's formatting looks approximately like you'd expect the output
  text to look.

---

count: false

# Some features of Subplot

- _Documentation_
  - Markdown text
  - **Diagrams as first class content**
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Not everyone is satisfied purely with words. Sometimes a diagram can be much
  easier to understand.
- Markdown supports arbitrary embedding of images, however images are not terribly
  easy to revision control.
- As such Subplot supports a number of textual diagram formats already, and more
  could be added.
- Currently Subplot supports the use of PlantUML for any of its UML diagram forms
- Subplot also supports `pikchr` which is a graph like language which describes
  flowchart type diagrams
- Subplot supports `dot` which is a more generic graph like language often used
  to show simple relationships, `dot` actually underlies PlantUML's output I believe.
- Each of these formats allows document authors to more easily revision control their
  diagrams and understand differences from version to version.

---

count: false

# Some features of Subplot

- _Documentation_
  - Markdown text
  - Diagrams as first class content
  - **PDF generation for archival**
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Since oftentimes you need to archive your argumentation as part of your release process,
  or to support later inspection by auditors, Subplot offers PDF rendering.
- This means that auditors need not understand Markdown nor the text forms of the diagrams,
  instead being able to view a fully rendered document.
- We are aware of some users of Subplot who only use it for rendering documents because
  they like the Markdown and diagram-as-text support

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - **HTML generation for ease of reading/flexible consumption**
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Even more widely supported as a document format is HTML.
- Subplot can render your documents as stand-alone HTML which can then be published
  onto websites, into document management systems, or simply stored for viewing without
  need for PDF software.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- **Code generation**
  - Generate your test suite
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- If we were only talking about a tool for nicely rendering documentation
  then we could stop here, but Subplot has a second major function.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- _Code generation_
  - **Generate your test suite**
  - Step libraries
  - Flexible templating
- Bootstrapping principle

???

- Unlike many tools, including Yarn, Subplot does not act as a test
  runner or orchestrator.
- Instead, Subplot can be used to _generate_ source code which runs
  the tests.
- Subplot can convert all the scenarios in a document into code which
  can run under more traditional test runners in order to be maximally
  flexible.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- _Code generation_
  - Generate your test suite
  - **Step libraries**
  - Flexible templating
- Bootstrapping principle

???

- To permit Subplot to actually understand and generate appropriate
  code, your document will list some number of step libraries in its
  metadata.
- Subplot uses those step libraries, through a data model we call
  bindings, to determine how to turn the scenarios into code to run.
- Subplot actually provides a number of standardised step libraries,
  including those which can run commands, read and write files, and
  a variety of other features.
- You can, and indeed likely will need to, write step libraries for your
  projects. They may vary in complexity and implementation "level"
  as needed to fulfil your needs for verifying your requirements.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- _Code generation_
  - Generate your test suite
  - Step libraries
  - **Flexible templating**
- Bootstrapping principle

???

- Finally, Subplot's code generation is templated and fairly flexible.
- We provide, as standard, a Python test runner which is very basic,
  but also very easy to use.
- The next release of Subplot will include support for generating Rust
  test suites which fit directly into Rust's testing model, making it
  easy to integrate Subplot scenarios into testing Rust code.
- We provide a basic shell (bash) based test suite template as well,
  though that is not as fully featured nor fully supported.
- As with step libraries, projects can provide their own templates,
  for example if you are verifying a component written in Typescript,
  or Ruby, you might provide a template which generates tests consumable
  by appropriate test runners for those languages.

---

count: false

# Some features of Subplot

- Documentation
  - Markdown text
  - Diagrams as first class content
  - PDF generation for archival
  - HTML generation for ease of reading/flexible consumption
- Code generation
  - Generate your test suite
  - Step libraries
  - Flexible templating
- **Bootstrapping principle**

???

- You may have heard the phrase "Eat your own dogfood"
- In this instance I'd prefer to say "Use your own tools"
- Subplot's primary set of verification critera are written as
  scenarios in Subplot's own documentation.
- This means that we use Subplot to test Subplot meets its requirements.
- Interestingly, we do this in all languages/test-suite styles that Subplot
  supports, where appropriate.
- We feel that this principle is important since it shows real-world
  use of the technique, and while I'll be the first to admit that we're
  not yet great at writing the documents ourselves, we're learning and
  improving all the time, through forced use of the technology for ourselves.

---

# The state of the Subplot project

- Still Alpha software
- Actively seeking interested parties
  - To help directing the project
  - To help developing the project
- Free software, hosted on GitLab
- Uses other free software projects
- Open decision making processes
- Written in Rust, with Bash, Python, and Rust code targets

???

- In summary, Subplot, despite being many years since that fateful lunch,
  is what I'd describe as alpha-grade software
- Currently run by two people - myself and Lars Wirzenius
- We have had contributions from some of our friends, including the third
  person at the original inception lunch
- We are painfully aware that two people cannot possibly represent the full
  gamut of possible users, nor use-cases. If this sounds interesting, we'd
  love to chat with you about how you can help.

---

count: false

# The state of the Subplot project

- Still Alpha software
- Actively seeking interested parties
  - To help directing the project
  - To help developing the project
- Free software, hosted on GitLab
- Uses other free software projects
- Open decision making processes
- Written in Rust, with Bash, Python, and Rust code targets

???

- Subplot is under the MIT licence to make it as easy as possible to integrate
  with both open and closed source workflows
- Rather than re-implementing the world, Subplot uses Pandoc for rendering the
  PDFs and HTML, PlantUML, dot, and pikchr for diagramming, and any number of
  open source libraries to achieve its goals.
- The project's decision making process consists of fortnightly meetings where
  issues are discussed, designs are drawn up, etc. These meetings are minuted
  on the Subplot website. Larger design goals are tackled one at a time, and
  we are currently working on our second such goal, the bringing of our Rust
  template up to par in order that we can consider it supported by default.
- Subplot is written in Rust, though as I mentioned, we have a number of
  language test-suite templates built into the tool, the most mature of which
  is the Python one.
- We'd love for you to come and join us and help Subplot grow to be more useful
  for the safety community.

---

layout: true
title: Subplot - Expressing argumentation in a validatable way

.bottom-bar[
{{title}}
]

---

class: impact

## Roundtable discussion idea:

## Using scenarios in safety argumentation

???

- Perhaps when we're through with any questions any of you have about Subplot,
  we might have a discussion in the channel about safety argumentation and
  automated validation of assertions.
- Do any of you think Subplot, or a similar tool, might have a place in making
  safety argumentation validation a more continuous process?

---

class: impact

# Any questions?

## <https://gitlab.com/subplot/subplot>

## <https://subplot.liw.fi/>

???

- Until then…
- Thank you for listening.
